#include "WindowManager.h"

ushort WindowManager::ConsoleColumns = 0;
ushort WindowManager::ConsoleRows = 0;
std::ostream *WindowManager::OutputStream = &std::cout;

void WindowManager::SetOutput(std::ostream* pOutput)
{
	OutputStream = pOutput;
}
void WindowManager::ClearStdBuffer()
{
	std::cin.clear();
	std::cin.ignore(std::cin.rdbuf()->in_avail());
}

void WindowManager::ClearScreen()
{
	system("cls");		// we are in windows terminal
    CONSOLE_SCREEN_BUFFER_INFO *ConsoleScreenBufferInfo = new CONSOLE_SCREEN_BUFFER_INFO; 
	// obtaining terminal size (Rows, Columns)
    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), ConsoleScreenBufferInfo);
    ConsoleColumns = ConsoleScreenBufferInfo->srWindow.Right - ConsoleScreenBufferInfo->srWindow.Left + 1;
    ConsoleRows = ConsoleScreenBufferInfo->srWindow.Bottom - ConsoleScreenBufferInfo->srWindow.Top + 1;
	
	delete ConsoleScreenBufferInfo;
}

void WindowManager::SetTitle(std::string pTitle)
{
	system(("title " + pTitle).c_str());
}

void WindowManager::SetColor(WORD pColor)
{
	HANDLE tHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(tHandle, pColor);
}

void WindowManager::Display(std::string pMessage)
{
	std::cout << pMessage;
}

void WindowManager::InitGraph()
{
	*OutputStream << std::setw(7) << "Time" << std::setw(4) << "|" << std::setw(39) << "Amlitude" << "\n";
	*OutputStream << "----------+--------------------------------------------------------------------\n";
}
void WindowManager::PlotGraph(std::vector<DataPair> pDataSet, float pMax, float pMin)
{
	InitGraph();
	std::vector<DataPair>::iterator DataSetIterator = pDataSet.begin();
	for (; DataSetIterator != pDataSet.end(); DataSetIterator++)
	{
		WindowManager::ShowGraphLine(*DataSetIterator, FindLocation(*DataSetIterator, pMax, pMin));
	}
}
uint WindowManager::FindLocation(DataPair pDataPair, float pMax, float pMin)
{
	// 69 chars are left for the point
	float tDelta = abs((pMin - pMax) / CHARS_FOR_VALUE);
	return (uint)((pDataPair.GetAmplitude() - pMin) / tDelta);
}
void WindowManager::ShowGraphLine(DataPair pDataPair, uint pLocation)
{
	// console window width = 80 chars
	std::stringstream FtoS;
	FtoS << pDataPair.GetAmplitude();
	std::string PointValue = FtoS.str();

	if ((CHARS_FOR_VALUE - pLocation) > PointValue.length())
		*OutputStream << std::setw(8) << pDataPair.GetTime() << "ms|" << std::setw(pLocation) << "*" << pDataPair.GetAmplitude() << "\n";
	else
		*OutputStream << std::setw(8) << pDataPair.GetTime() << "ms|" << std::setw(pLocation - 2) << pDataPair.GetAmplitude() << "*" << "\n";
}