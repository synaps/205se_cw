﻿#include "AMCore.h"

// Constructor
AMCore::AMCore(ushort pNumberOfFiles)
{
	NumberOfFiles = pNumberOfFiles;
}
AMCore::~AMCore()
{
	std::vector<std::vector<DataPair*>>::iterator ListIterator;
	for (ListIterator = Files.begin(); ListIterator != Files.end(); ListIterator++)
	{
		std::vector<DataPair*>::iterator DataPairIterator;
		for (DataPairIterator = (*ListIterator).begin(); DataPairIterator != (*ListIterator).end(); DataPairIterator++)
			delete *DataPairIterator;
	}
}

bool AMCore::ReadFile(std::string pFilename)
{
	if(Files.size() > NumberOfFiles)
		return false;
	// read and parse file

	std::fstream File;
	// failbit = 1 if cannot open file
	File.exceptions(std::ifstream::badbit | std::ifstream::failbit);
	try
	{
		File.open(pFilename.c_str(), std::ios::in);
		std::vector<DataPair*> tFile;
		
		// readline sets EOF & FAILBIT on the end of file (we don't want that)
		File.exceptions(std::ifstream::badbit);
		
		std::string Line;
		while(std::getline(File, Line))
		{			
			float Time, Amplitude;
			if (!AMCore::ReadLine(&Time, &Amplitude, Line))
				break;
			tFile.push_back(new DataPair(Time, Amplitude));
		}
		Files.push_back(tFile);
		return true;
    } catch(const std::exception &e) // known bug that we cannot catch std::ios_base::failure exception (gcc)
    {
		WindowManager::SetColor(FOREGROUND_RED | FOREGROUND_INTENSITY);
		std::cout << "You have mistyped the file name or file is not present in the filesystem.\n";
		std::cout << "-------------------------------------------------------------------------\n";
		std::cout << "Caught std::exception: " << typeid(e).name() << '\n';
		printf("badbit = %d, failbit = %d, eof = %d\n", File.bad(), File.fail(), File.eof());
		std::cout << "-------------------------------------------------------------------------\n";
		WindowManager::SetColor(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		
		return false;
    }
	File.close();
}

bool AMCore::ReadLine(float *pTime, float *pAmplitude, std::string pLine)
{
	std::stringstream tLine(pLine);
	tLine >> *pTime >> *pAmplitude;
	return true;
}

std::vector<DataPair> AMCore::MultiplyDataPairs()		// тут один большой костыль...
{
	std::vector<DataPair> rData;
	for (uint LineIndex = 0; LineIndex < Files.front().size(); LineIndex++)
	{
		DataPair tDataPair(1, 1);		// а конкретно тут
		for (uint FileIndex = 0; FileIndex < Files.size(); FileIndex++)
		{
			tDataPair.SetTime((*Files[FileIndex][LineIndex]).GetTime());		// обходим ошибку при неравном времени
			tDataPair = tDataPair * *Files[FileIndex][LineIndex];
		}
		rData.push_back(tDataPair);
	}
	ResultingSet = rData;
	return rData;
}
std::vector<DataPair> AMCore::GetResultingSet()
{
	return ResultingSet;
}

bool AMCore::FindExtrema(float *pMax, float *pMin, std::vector<DataPair> pArray)
{
	if (pArray.size() == 0)
		pArray = ResultingSet;
	*pMax = pArray[0].GetAmplitude();
	*pMin = pArray[0].GetAmplitude();
	std::vector<DataPair>::iterator ArrayIterator = pArray.begin();
	for (; ArrayIterator != pArray.end(); ArrayIterator++)
	{
		float tAmplitude = ArrayIterator->GetAmplitude();
		if (tAmplitude > *pMax)
			*pMax = tAmplitude;
		if (tAmplitude < *pMin)
			*pMin = tAmplitude;
	}
	return true;
}

std::vector<DataPair> AMCore::SetScale(std::vector<DataPair> pDataSet, uint pScale)
{
	std::vector<DataPair> rDataSet;
	uint Element = round(pDataSet.size() / (float)pScale);
	for (uint Index = 0; Index < pDataSet.size(); Index++)
	{
		if (Index % Element != 0)
			continue;
		rDataSet.push_back(pDataSet[Index]);
	}
	ResultingSet = rDataSet;
	return rDataSet;
}

void AMCore::SaveAsFile(std::string pFileName, std::vector<DataPair> pDataSet, float pMax, float pMin)
{
	std::ofstream OutputFile(pFileName);
	WindowManager::SetOutput(&OutputFile);
	WindowManager::PlotGraph(pDataSet, pMax, pMin);
	WindowManager::SetOutput(&std::cout);
	OutputFile.close();
}