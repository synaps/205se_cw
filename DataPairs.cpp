#include <DataPairs.h>

DataPair operator*(DataPair pFirst, DataPair pSecond)
{
	return DataPair(pFirst.Time, pFirst.Amplitude * pSecond.Amplitude); 
}
	
DataPairs::DataPairs()
{
	
}

DataPairs::~DataPairs()
{
	std::vector<DataPair*>::iterator ListIterator;
	for(ListIterator = Data.begin(); ListIterator != Data.end(); ListIterator++)
		delete *ListIterator;
}

unsigned int DataPairs::NumberOfEntries()
{
	return Data.size();
}

DataPair* DataPairs::DataPairAt(unsigned int pIndex)
{
	return Data[pIndex];
}

void DataPairs::Add(DataPair* pDataPair)
{
	Data.push_back(pDataPair);
}