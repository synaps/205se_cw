#ifndef DATAPAIRS_H
#define DATAPAIRS_H

#include <string>
#include <vector>

struct DataPair
{
	float Time, Amplitude;
	
	// Constructor
	DataPair(float pTime, float pAmplitude) : Time(pTime), Amplitude(pAmplitude) {}
	
	std::string ToString()
	{
		return std::string("Time: " + std::to_string(Time) + ", Amplitude: " + std::to_string(Amplitude));
	}
};

class DataPairs
{
private:
	std::vector<DataPair*> Data;		// Represents lines in files
public:
	DataPairs();
	~DataPairs();
	
	unsigned int NumberOfEntries();
	DataPair* DataPairAt(unsigned int);
	void Add(DataPair*);
};

#endif