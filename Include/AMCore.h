#ifndef AMCORE_H
#define AMCORE_H

// STD
#include <vector>	// vector is faster than list until we want to sort it
#include <list>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>	// using stingstream instead of regex 
#include <typeinfo>
#include <cmath>

#include "WindowManager.h"
#include "DataPair.h"
#include "Variables.h"

class AMCore
{
private:
	// variables
	std::vector<std::vector<DataPair*>> Files;		// Represents different files
	std::vector<DataPair> ResultingSet;
	ushort NumberOfFiles;

	static bool ReadLine(float*, float*, std::string);
public:
	// constructors & destructors
	AMCore(ushort = 2);
	~AMCore();
	// Functions
	bool ReadFile(std::string);
	std::vector<DataPair> MultiplyDataPairs();
	bool FindExtrema(float*, float*, std::vector<DataPair>);
	std::vector<DataPair> SetScale(std::vector<DataPair>, uint);
	void SaveAsFile(std::string, std::vector<DataPair>, float, float);
	// Accesors
	std::vector<DataPair> GetResultingSet();
	// operators
};

#endif