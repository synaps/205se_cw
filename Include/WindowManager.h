#ifndef WINDOWMANAGER_H
#define WINDOWMANAGER_H

// Windows only
#include <windows.h>
// STD
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cmath>
// own
#include "Variables.h"
#include "DataPair.h"

#define CHARS_FOR_VALUE 69

class WindowManager
{
private:
	static ushort ConsoleColumns, ConsoleRows;
	static std::ostream* OutputStream;
	
	WindowManager(){};
	~WindowManager(){};

	static void InitGraph();
	static void ShowGraphLine(DataPair, uint);
	static uint FindLocation(DataPair, float, float);

public:
	static void SetOutput(std::ostream*);
	static void ClearStdBuffer();
	static void SetColor(WORD);
	static void ClearScreen();
	static void SetTitle(std::string);
	static void Display(std::string);
	static void PlotGraph(std::vector<DataPair>, float, float);
};

#endif