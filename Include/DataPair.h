#ifndef DATAPAIRS_H
#define DATAPAIRS_H

#include <string>
#include <vector>
#include <algorithm>
#include <exception>
#include <sstream>

#include "Variables.h"


class DataPair
{
private:
	float Time, Amplitude;
public:
	// constructors & destructors
	DataPair(float, float);
	~DataPair();
	// Accesors
	float GetTime();
	float GetAmplitude();
	// Functions
	std::string ToString();
	void SetTime(float);
	// operators
	DataPair& operator*(DataPair);
};

#endif