// STD
#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <climits>
// own
#include "AMCore.h"
#include "WindowManager.h"
#include "DataPair.h"
#include "Signal.h"
#include "Variables.h"

int main()
{
	bool Repeat = false;
	do
	{
		WindowManager::SetTitle("205SE. Rustam Rahimgulov. 5810345");
		WindowManager::ClearScreen();
		WindowManager::SetColor(FOREGROUND_GREEN | FOREGROUND_INTENSITY);
		WindowManager::Display("205SE_CW Rustam Rahimgulov 5810345\n");
		WindowManager::SetColor(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);

		ushort *NumberOfFiles = new ushort;
		*NumberOfFiles = 0;
		WindowManager::Display("Specify number of files in range from 2 to 99: \n");
		do
		{
			WindowManager::SetColor(FOREGROUND_BLUE | FOREGROUND_GREEN);
			std::cin >> *NumberOfFiles;
			std::cin.clear();
			std::cin.ignore(std::cin.rdbuf()->in_avail());
			WindowManager::SetColor(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);

			if ((*NumberOfFiles < 2) || (*NumberOfFiles > 99))
			{
				WindowManager::Display("Seems that input is wrong!\n");
			}
		} while ((*NumberOfFiles < 2) || (*NumberOfFiles > 99));

		AMCore *ModelManager = new AMCore(*NumberOfFiles);

		std::string Filename;
		while (*NumberOfFiles > 0)
		{
			do
			{
				WindowManager::Display("Please enter filename:\n");
				WindowManager::SetColor(FOREGROUND_BLUE | FOREGROUND_GREEN);
				std::cin >> Filename;
				WindowManager::SetColor(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			} while (!ModelManager->ReadFile(Filename));
			--*NumberOfFiles;
			WindowManager::Display("File has been read succesfully.\n");
		}
		delete NumberOfFiles;
		std::vector<DataPair> ResultingSet = ModelManager->MultiplyDataPairs();

		std::stringstream *tStringBuilder = new std::stringstream();
		*tStringBuilder << "How many lines do you want to see (out of " << ResultingSet.size() << " elements)?\n";
		std::string LinesQString = tStringBuilder->str();
		delete tStringBuilder;
		uint Lines = 0;
		do
		{
			WindowManager::Display(LinesQString);
			WindowManager::SetColor(FOREGROUND_BLUE | FOREGROUND_GREEN);
			std::cin >> Lines;
			WindowManager::SetColor(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			WindowManager::ClearStdBuffer();
		} while (Lines > ResultingSet.size() || Lines == 0);

		ResultingSet = ModelManager->SetScale(ResultingSet, Lines);

		float Max, Min;		// Extremum
		if (!ModelManager->FindExtrema(&Max, &Min, ResultingSet))
			throw std::logic_error("Extremum weren't found.");

		WindowManager::PlotGraph(ResultingSet, Max, Min);

		char *tAnswer = new char;
		do
		{
			WindowManager::Display("Do you want to save the output to the file? [y/n]");
			WindowManager::SetColor(FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
			WindowManager::ClearStdBuffer();
			std::cin >> *tAnswer;
			WindowManager::ClearStdBuffer();
			WindowManager::SetColor(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			if (*tAnswer == 'y' || *tAnswer == 'Y')
			{
				std::string Filename;
				WindowManager::Display("Enter file name of the output file: ");
				WindowManager::SetColor(FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
				WindowManager::ClearStdBuffer();
				std::cin >> Filename;
				WindowManager::ClearStdBuffer();
				WindowManager::SetColor(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
				ModelManager->SaveAsFile(Filename, ResultingSet, Max, Min);
				WindowManager::Display("The amplitude modulation has been saved as " + Filename + "\n");
				break;
			}
			else if (*tAnswer == 'n' || *tAnswer == 'N')
				break;
		} while (true);

		do
		{
			WindowManager::Display("Do you want to rerun the program? [y/n]");
			WindowManager::SetColor(FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
			WindowManager::ClearStdBuffer();
			*tAnswer = std::cin.get();
			WindowManager::ClearStdBuffer();
			WindowManager::SetColor(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			if (*tAnswer == 'y' || *tAnswer == 'Y')
			{
				Repeat = true;
				break;
			}
			else if (*tAnswer == 'n' || *tAnswer == 'N')
			{
				Repeat = false;
				WindowManager::Display("Thank you for using the amplitude modulation program. \nGoodbye!");
				break;
			}
		} while (true);
		
		delete tAnswer;

		delete ModelManager;
	} while (Repeat);

	return 0;
}
