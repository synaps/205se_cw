#include "DataPair.h"
	
DataPair::DataPair(float pTime, float pAmplitude)
{
	if (pTime < 0)
		throw std::logic_error("Time supplied is smaller than 0");
	Time = pTime;
	Amplitude = pAmplitude;
}
DataPair::~DataPair()
{}

float DataPair::GetTime()
{
	return Time;
}
float DataPair::GetAmplitude()
{
	return Amplitude;
}

void DataPair::SetTime(float pTime)
{
	Time = pTime;
}
std::string DataPair::ToString()
{
	std::stringstream StringBuilder;
	StringBuilder << "Time: " << Time << ", Amplitude: " << Amplitude;
	return StringBuilder.str();
}

DataPair& DataPair::operator*(DataPair pParameter)
{
	if (this->Time != pParameter.Time)
		throw std::logic_error("Time is not equal.");
	this->Amplitude *= pParameter.Amplitude;
	return *this;
}